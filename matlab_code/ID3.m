load ("rice_dataset");

Osc = RiceOsmancikCammeoDataset.CLASS;

%We decided to make Cammeo positive, and Osmancik negative

positive = 0;
negative = 0;

for i = 1:size(Osc)
    if(Osc(i,:) == "Cammeo")
        positive = positive + 1;
    else 
        negative = negative + 1;
    end 
end

%positive
%negative

%This would give the Entropy for the Entire Dataset
%EntropyFullData(positive, negative)

function output = EntropyFullData(p, n)
    
    output = -(p/(p+n)) * log2( p/(p+n) ) - (n/(p+n)) * log2(n/(p+n));
    
end


function output = EntropyRemainder(p, n, attribute)
    
    

end

function output = EntropyGain(p, n, attribute)

    output = EntropyFullData(p,n) - EntropyRemainder(p,n,attribute);

end
