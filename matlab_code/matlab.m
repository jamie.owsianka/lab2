%% Regression Tree
load('wine_data/winequalityred.mat');
Y = winequalityred.quality;    % LABEL - change to a smaller subset for polynomial, e.g. winequalityred.quality(1:450)
X = winequalityred(:,1:end-1); % FEATURES - change to a smaller subset for polynomial, e.g. winequalityred(1:450,1:end-1)
global attributes;
attributes = X.Properties.VariableNames(1:end);

% run tree
t1 = decision_tree_learning(X, Y);
index = 1;

function tree = new_tree(name, prediction, attribute, threshold) 
    tree = struct('op', name, 'kids', ([]*2),  'prediction', prediction, 'attribute', attribute, 'threshold', threshold);
end

function [best_attribute, best_threshold] = choose_attribute_classification(X, Y) 
    best_attribute = 1;
    best_threshold = 1;
    % TO DO
end


function [best_attribute, best_threshold] = choose_attribute_regression(X, Y) 
    best_attribute = 1;
    best_threshold = 1;
    % TO DO
end

function [x_left, y_left] = get_examples_left(X, Y, best_attribute, best_threshold)
    
    x_left = {};
    y_left = [];
    index = 1;
    
    % {elements of examples with best_attribute < best_threshold and corresponding target labels}
    for n = 1:(size(X))
        features = table2array(X(n,:));
        label = Y(n);
        fprintf("\nleft - f: %d, bestThreshold: %d",features(:,best_attribute), best_threshold);
        
        if (features(:,best_attribute) < best_threshold)
            x_left{index} = features;
            y_left(index) = label;
        end
        index = index + 1;
    end
    
    for n = 1:(size(y_left))
        dprintf("\ny_left: %d", y_left(n)); 
    end

end

function [x_right, y_right] = get_examples_right(X, Y, best_attribute, best_threshold)
        
    x_right = {};
    y_right = [];
    index = 1;
    
    % {{elements of examples with best_attribute >=  best_threshold and corresponding target labels}
    for n = 1:(size(X))
        features = table2array(X(n,:));
        label = Y(n);
        fprintf("\nright - f: %d, bestThreshold: %d",features(:,best_attribute), best_threshold);
        
        if (features(:,best_attribute) >= best_threshold)
            x_right{index} = features;
            y_right(index) = label;
        end
        index = index + 1;
    end
    
    for n = 1:(size(y_right))
        fprintf("\ny_right: %d", y_right(n)); 
    end


end

%{
function DECISION-TREE-LEARNING(features, labels) returns a decision tree
inputs: features, set of N x d training examples
labels, set of N x 1 target labels for the training examples
%}

function tree = decision_tree_learning(X, Y) 
    
    global attributes;

    if (size(Y) <= 0) 
        return; 
    end
       
    % if all examples have the same label then return a leaf node with label = the label
    label = Y(1);
    same_label = 1;


    for n = 1:size(Y) 
        %fprintf("\nY(n): %d, label %d, sizeY: %d",Y(n),label,size(Y));
        
        if (Y(n) ~= label) 
            same_label = 0;
        end
    end

    if (same_label == 1) 
        fprintf("returning: same label"); 
        tree = new_tree("error", 0.00, 1, 1);
        return
    end

    if (same_label == 0)
        % [best_attribute, best_threshold] ? CHOOSE-ATTRIBUTE(features,targets)
        [best_attribute, best_threshold] = choose_attribute_classification(X, Y); 

        % tree ? a new decision tree with root decision attribute best_attribute and threshold best_threshold
        tree = new_tree(attributes(best_attribute), 0.00, best_attribute, best_threshold);

        % add a branch to tree corresponding to best_attribute < best_threshold
        if (best_attribute < best_threshold)

            % {examples.l , targets.l}? {elements of examples with best_attribute < best_threshold and corresponding target labels}
            [x_left, y_left] = get_examples_left(X, Y, best_attribute, best_threshold);

            % subtree ? DECISION-TREE-LEARNING(examplesl , targets)
            sub_tree = decision_tree_learning(x_left, y_left); 
            tree.kids(1) = sub_tree;

        % add a branch to tree corresponding to best_attribute >= best_threshold
        else 

            % {examples.r , targets.r}? {elements of examples with best_attribute >=  best_threshold and corresponding target labels}
            [x_right, y_right] = get_examples_right(X, Y, best_attribute, best_threshold);

            % subtree ? DECISION-TREE-LEARNING(examplesr , targets)
            sub_tree = decision_tree_learning(x_right, y_right); 
            tree.kids(2) = sub_tree;


        end
    end

end
