%% GET TABLES
load('wine_data/winequalityred.mat');
Y = winequalityred.quality; % LABEL - change to a smaller subset for polynomial, e.g. winequalityred.quality(1:450)
winequalityred = winequalityred(:,1:end-1); % FEATURES - change to a smaller subset for polynomial, e.g. winequalityred(1:450,1:end-1)
%Y = winequalityred.quality(1:450);
%winequalityred= winequalityred(1:450,1:end-1);
%% REMOVE OUTLIERS
%{
"clip" Method from MATLAB
Fills with the lower threshold value for elements smaller than the lower threshold determined by findmethod. 
Fills with the upper threshold value for elements larger than the upper threshold determined by findmethod
%}
redwine_rmoutl = filloutliers(winequalityred,'clip','percentiles',[5 95]);

%% NORMALISE
% scale values
X = normalize(redwine_rmoutl,'range'); % default range [0 1]

%% SPLIT THE DATA FOR CROSS VALIDATION

crossDataX = {}; % FEATURES
crossDataY = {}; % LABEL
for n=1:10 
    crossDataX{n} = X((round(((n-1)/10)*size(X,1)) +1):(round((n/10)*size(X,1))),:);
    crossDataY{n} = Y((round(((n-1)/10)*size(Y,1)) +1):(round((n/10)*size(Y,1))),:);
end

%% CROSS VALIDATION

err = 10*[];
supVec = 10*[];

for n=1:10 
    trainingX = [];
    trainingY = [];
    %create each training group
    for i = 1:10
        if i ~= n
            trainingX = [trainingX ; crossDataX{i}];
            trainingY = [trainingY ; crossDataY{i}];
        end
    end
    Mdl = innerCVGaussian(trainingX, trainingY, 0.005, 1, 0.005); 
    % Mdl = innerCVPolynomial(trainingX, trainingY, 0.005, 1, 0.5);
    % Mdl = innerCVLinear(trainingX, trainingY, 0.005);
    
    err(n) = performance(Mdl, crossDataX{n}, crossDataY{n});
    supVec(n) = size(Mdl.SupportVectors,1);
end

fprintf('\n\nCROSS VALIDATION RESULTS');

for n=1:10
    fprintf('\nPartition %i:', n);
    fprintf('\n-- Mean squared error: %f', err(n));
    fprintf('\n-- Support vectors number: %f', supVec(n));
    fprintf('\n-- Support vectors %%: %f', supVec(n)/size(trainingX,1)*100);
end

fprintf('\n\nAVERAGE VALUES');
fprintf('\n-- Average MSE: %f', mean(err));
fprintf('\n-- Average SV number: %f', mean(supVec));
fprintf('\n-- Average SV %%: %f', mean(supVec)/size(trainingX,1)*100);

%% FUNCTIONS

function bestMdl = innerCVLinear(X, Y, epsilon_step)
    % INNER CV LINEAR FUNCTION
    % Hyperparameters: Epsilon

    %% SPLIT DATA (70/30)

    rand_num = randperm(size(X,1));

    x_train = X(rand_num(1:round(0.7*length(rand_num))),:);
    y_train = Y(rand_num(1:round(0.7*length(rand_num))),:);

    x_test = X(rand_num(round(0.7*length(rand_num))+1:end),:);
    y_test = Y(rand_num(round(0.7*length(rand_num))+1:end),:);
    
    %% INITIALISE VALUES

    iteration = 1;

    epsilon = rand(1,1)/10;

    improved_epsilon = true;

    goUp = false;

    %% GET INITIAL SOLUTION

    Mdl = newLinearModel(x_train, y_train, epsilon);
    bestMdl = Mdl;
    
    best_performance = performance(Mdl,x_test,y_test);
    
    %% EPSILON HILL CLIMBING

    % First, find best out of ten epsilon values in
    % the following ranges [0,0.1],[0.1,0.2]..[0.9,1],[0-111].

    for e = 1:10
        current_performance = performance(bestMdl,x_test,y_test);
        if (e == 10)
            temp_epsilon = rand(1,1)*100 + rand(1,1)*10 + rand(1,1);
        else
            temp_epsilon = rand(1,1)/10 + e/10;
        end
        Mdl = newLinearModel(x_train, y_train, temp_epsilon);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        % if new epsilon value has better performance
        if new_performance < current_performance
            
            if e == 10      
                epsilon_step = 0.5;
            end
            epsilon = temp_epsilon;
            best_performance = new_performance;
            bestMdl = Mdl;
        end
    end

    while (improved_epsilon)

        current_performance = performance(bestMdl,x_test,y_test);
        Mdl = newLinearModel(x_train, y_train, epsilon + epsilon_step);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        % if higher epsilon value has better performance
        if new_performance < current_performance

            epsilon = epsilon + epsilon_step;
            best_performance = new_performance;
            goUp = true;
            bestMdl = Mdl;
        else
            improved_epsilon = false;
        end
    end

    if (goUp == false)

        improved_epsilon = true;
        while (improved_epsilon)
            if ((epsilon - epsilon_step) <= 0)
                break
            end

            current_performance = performance(bestMdl,x_test,y_test);
            Mdl = newLinearModel(x_train, y_train, epsilon - epsilon_step);
            iteration = iteration + 1;
            new_performance = performance(Mdl,x_test,y_test);

            if new_performance < current_performance

                epsilon = epsilon - epsilon_step;
                best_performance = new_performance;
                bestMdl = Mdl;

            else 
                improved_epsilon = false;
            end
        end
    end

    fprintf("\nEpsilon local minimum found: %f with accuracy: %f", epsilon, best_performance);

end

function bestMdl = innerCVGaussian(X, Y, epsilon_step, box_constraint_step, sigma_step)
    % INNER CV GAUSSIAN FUNCTION
    % Hyperparameters: (q) PolynomialOrder, Epsilon, (sigma) Kernel Scale

    %% SPLIT DATA (70/30)

    rand_num = randperm(size(X,1));

    x_train = X(rand_num(1:round(0.7*length(rand_num))),:);
    y_train = Y(rand_num(1:round(0.7*length(rand_num))),:);

    x_test = X(rand_num(round(0.7*length(rand_num))+1:end),:);
    y_test = Y(rand_num(round(0.7*length(rand_num))+1:end),:);
    
    %% INITIALISE VALUES

    iteration = 1;

    fprintf('|=========================================================================================================================================|\n');
    fprintf('     Iter | Eval   | Objective:  | BestSoFar   | BoxConstraint |    Kernel    |  Epsilon  | Support Vectors(n) | Support Vectors(%%) |\n');
    fprintf('          | result |             | (observed)  |      (C)      |    Scale     |           |                    |                    |\n');
    fprintf('|=========================================================================================================================================|');

    epsilon = rand(1,1)/10;
    box_constraint = randi([1 15],1);
    sigma = rand(1,1)/10;

    improved_epsilon = true;
    improved_boxConstraint = true;
    improved_sigma = true;

    goUp = false;

    %% GET INITIAL SOLUTION

    Mdl = newGausModel(x_train, y_train, epsilon, box_constraint, sigma);
    bestMdl = Mdl;
    
    isSupportVector = size(Mdl.IsSupportVector);
    selectedVectors = size(Mdl.SupportVectors);
    supportVectors1 = selectedVectors(1);
    supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;
    
    best_performance = performance(Mdl,x_test,y_test);
    printBest(iteration, best_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);
    %fprintf('\n| %i | %f | best | %f | %f | %f | %f | %f | %f | ', iteration, best_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);

    %% EPSILON HILL CLIMBING

    % First, find best out of ten epsilon values in
    % the following ranges [0,0.1],[0.1,0.2]..[0.9,1],[0-111].

    for e = 1:10
        current_performance = performance(bestMdl,x_test,y_test);
        if (e == 10)
            temp_epsilon = rand(1,1)*100 + rand(1,1)*10 + rand(1,1);
        else
            temp_epsilon = rand(1,1)/10 + e/10;
        end
        Mdl = newGausModel(x_train, y_train, box_constraint, temp_epsilon , sigma);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if new epsilon value has better performance
        if new_performance < current_performance
            
            if e == 10      
                epsilon_step = 0.5;
            end
            epsilon = temp_epsilon;
            best_performance = new_performance;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);
        else
            printReject(iteration, new_performance, best_performance, box_constraint, sigma, temp_epsilon, supportVectors1, supportVectors2);
        end
    end

    while (improved_epsilon)

        current_performance = performance(bestMdl,x_test,y_test);
        Mdl = newGausModel(x_train, y_train, box_constraint, epsilon + epsilon_step , sigma);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if higher epsilon value has better performance
        if new_performance < current_performance

            epsilon = epsilon + epsilon_step;
            best_performance = new_performance;
            goUp = true;
            bestMdl = Mdl;
            printAccept(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);
        else
            printReject(iteration, new_performance, best_performance, box_constraint, sigma, epsilon + epsilon_step, supportVectors1, supportVectors2);
            improved_epsilon = false;
        end
    end

    if (goUp == false)

        improved_epsilon = true;
        while (improved_epsilon)
            if ((epsilon - epsilon_step) <= 0)
                break
            end

            current_performance = performance(bestMdl,x_test,y_test);
            Mdl = newGausModel(x_train, y_train, box_constraint, epsilon - epsilon_step, sigma);
            iteration = iteration + 1;
            new_performance = performance(Mdl,x_test,y_test);

            selectedVectors = size(Mdl.SupportVectors);
            supportVectors1 = selectedVectors(1);
            supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

            if new_performance < current_performance

                epsilon = epsilon - epsilon_step;
                best_performance = new_performance;
                bestMdl = Mdl;
                printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);

            else 
                printReject(iteration, new_performance, best_performance, box_constraint, sigma, epsilon - epsilon_step, supportVectors1, supportVectors2);
                improved_epsilon = false;
            end
        end
    end

    fprintf("\nEpsilon local minimum found: %f with accuracy: %f", epsilon, best_performance);

    %% BOX CONSTRAINT HILL CLIMBING

    % First, find best out of five random values for the parameter

    for e = 1:4
        current_performance = performance(bestMdl,x_test,y_test);
        temp_box_constraint = randi([1 15],1);
        Mdl = newGausModel(x_train, y_train, temp_box_constraint, epsilon , sigma);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if new box constraint value has better performance
        if new_performance < current_performance

            box_constraint = temp_box_constraint;
            best_performance = new_performance;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);

        else
            printReject(iteration, new_performance, best_performance, temp_box_constraint, sigma, epsilon, supportVectors1, supportVectors2);
        end
    end

    goUp = false;

    while (improved_boxConstraint)

        current_performance = performance(bestMdl,x_test,y_test);
        Mdl = newGausModel(x_train, y_train, box_constraint + box_constraint_step, epsilon, sigma);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);
        
        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if higher epsilon value has better performance
        if new_performance < current_performance

            box_constraint = box_constraint + box_constraint_step;
            best_performance = new_performance;
            goUp = true;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);

        else
            printReject(iteration, new_performance, best_performance, box_constraint + box_constraint_step, sigma, epsilon, supportVectors1, supportVectors2);
            improved_boxConstraint = false;
        end
    end

    if (goUp == false)

        improved_boxConstraint = true;
        while (improved_boxConstraint)
            % decrement box constraint
            if ((box_constraint - box_constraint_step) < 1)
                break
            end

            current_performance = performance(bestMdl,x_test,y_test);
            Mdl = newGausModel(x_train, y_train, box_constraint - box_constraint_step, epsilon, sigma);
            iteration = iteration + 1;
            new_performance = performance(Mdl,x_test,y_test);
            
            selectedVectors = size(Mdl.SupportVectors);
            supportVectors1 = selectedVectors(1);
            supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

            if new_performance < current_performance

                box_constraint = box_constraint - box_constraint_step;
                best_performance = new_performance;
                bestMdl = Mdl;
                printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon,  supportVectors1, supportVectors2);

            else 
                % if decrement improves performance break
                % loop
                printReject(iteration, new_performance, best_performance, box_constraint - box_constraint_step, sigma, epsilon, supportVectors1, supportVectors2);
                improved_boxConstraint = false;
            end
        end
    end

    fprintf("\nBox constraint local minimum found: %f with accuracy: %f", box_constraint, best_performance);

    %% KERNEL SCALE HILL CLIMBING

    % First, find best out of ten epsilon values in
    % the following ranges [0,0.1],[0.1,0.2]..[0.9,1],[0-111],[0-1111].

    for e = 1:11
        current_performance = performance(bestMdl,x_test,y_test);
        if (e == 11)
            temp_sigma = rand(1,1)*1000 + rand(1,1)*100 + rand(1,1)*10 + rand(1,1);
        elseif (e == 10)
            temp_sigma = rand(1,1)*100 + rand(1,1)*10 + rand(1,1);
        else
            temp_sigma = rand(1,1)/10 + e/10;
        end
        Mdl = newGausModel(x_train, y_train, box_constraint, temp_sigma , sigma);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);
        
        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if new epsilon value has better performance
        if new_performance < current_performance

            if e == 10 || e == 11      
                sigma_step = 0.5;
            end
            sigma = temp_sigma;
            best_performance = new_performance;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);
        else
            printReject(iteration, new_performance, best_performance, box_constraint, temp_sigma, epsilon, supportVectors1, supportVectors2);
        end
    end

    goUp = false;

    while (improved_sigma)

        current_performance = performance(bestMdl,x_test,y_test);
        Mdl = newGausModel(x_train, y_train, box_constraint, epsilon, sigma + sigma_step);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);
        
        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if higher epsilon value has better performance
        if new_performance < current_performance

            sigma = sigma + sigma_step;
            best_performance = new_performance;
            goUp = true;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);

        else
            printReject(iteration, new_performance, best_performance, box_constraint, sigma + sigma_step, epsilon, supportVectors1, supportVectors2);
            improved_sigma = false;
        end
    end

    if (goUp == false)

        improved_sigma = true;
        while (improved_sigma)
            if ((sigma - sigma_step) <= 1)
                break
            end

            current_performance = performance(bestMdl,x_test,y_test);
            Mdl = newGausModel(x_train, y_train, box_constraint, epsilon, sigma - sigma_step);
            iteration = iteration + 1;
            new_performance = performance(Mdl,x_test,y_test);

            selectedVectors = size(Mdl.SupportVectors);
            supportVectors1 = selectedVectors(1);
            supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

            if new_performance < current_performance

                sigma = sigma - sigma_step;
                best_performance = new_performance;
                bestMdl = Mdl;
                printBest(iteration, new_performance, best_performance, box_constraint, sigma, epsilon, supportVectors1, supportVectors2);

            else 
                printReject(iteration, new_performance, best_performance, box_constraint, sigma + sigma_step, epsilon, supportVectors1, supportVectors2);
                improved_sigma = false;
            end
        end
    end

    fprintf("\nKernel scale local minimum found: %f with accuracy: %f", sigma, best_performance);

    fprintf('\n\nFINAL RESULT: \n');
    %fprintf('\nBest Performance: %f \nBest box constraint: %f \nBest kernel scale: %f \nBest epsilon: %f ', global_best_performance, global_best_box, global_best_sigma, global_best_epsilon);
    fprintf('\nBest Performance: %f \nBest box constraint: %f \nBest kernel scale: %f \nBest epsilon: %f ', best_performance, box_constraint, sigma, epsilon);

    %predictedLabels = predict(globalBestMdl,x_test); 
    predictedLabels = predict(bestMdl,x_test);
    modelAccuracy = sum((round(predictedLabels) == y_test))/length(y_test)*100;

    fprintf('\nAccuracy for rounded results (using percentages): %f', modelAccuracy);

end

function bestMdl = innerCVPolynomial(X, Y, epsilon_step, box_constraint_step, polyO_step)
    % INNER CV POLYNOMIAL FUNCTION
    % Hyperparameters: (q) PolynomialOrder, Epsilon, (c) Box Constraint

    %% SPLIT DATA (70/30)

    rand_num = randperm(size(X,1));

    x_train = X(rand_num(1:round(0.7*length(rand_num))),:);
    y_train = Y(rand_num(1:round(0.7*length(rand_num))),:);

    x_test = X(rand_num(round(0.7*length(rand_num))+1:end),:);
    y_test = Y(rand_num(round(0.7*length(rand_num))+1:end),:);
    
    %% INITIALISE VALUES

    iteration = 1;

    fprintf('|=========================================================================================================================================|\n');
    fprintf('     Iter | Eval   | Objective:  | BestSoFar   | BoxConstraint |  Polynomial  |  Epsilon  | Support Vectors(n) | Support Vectors(%%) |\n');
    fprintf('          | result |             | (observed)  |      (C)      |  Order       |           |                    |                    |\n');
    fprintf('|=========================================================================================================================================|');

    epsilon = rand(1,1)/10;
    box_constraint = randi([1 15],1);
    polyO = randi([1 10],1);

    improved_epsilon = true;
    improved_boxConstraint = true;
    improved_polyO = true;

    goUp = false;

    %% GET INITIAL SOLUTION

    Mdl = newModel(x_train, y_train, epsilon, box_constraint, polyO);

    bestMdl = Mdl;

    isSupportVector = size(Mdl.IsSupportVector);
    selectedVectors = size(Mdl.SupportVectors);
    supportVectors1 = selectedVectors(1);
    supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

    best_performance = performance(Mdl,x_test,y_test);

    printBest(iteration, best_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);
    
    %% EPSILON HILL CLIMBING

    % First, find best out of ten epsilon values in
    % the following ranges [0,0.1],[0.1,0.2]..[0.9,1],[0-111].

    for e = 1:10
        current_performance = performance(bestMdl,x_test,y_test);
        if (e == 10)
            temp_epsilon = rand(1,1)*100 + rand(1,1)*10 + rand(1,1);
        else
            temp_epsilon = rand(1,1)/10 + e/10;
        end
        Mdl = newModel(x_train, y_train, box_constraint, temp_epsilon , polyO);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if new epsilon value has better performance
        if new_performance < current_performance

            if e == 10      
                epsilon_step = 0.5;
            end
            epsilon = temp_epsilon;
            best_performance = new_performance;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);
        else
            printReject(iteration, new_performance, best_performance, box_constraint, polyO, temp_epsilon, supportVectors1, supportVectors2);
        end
    end

    while (improved_epsilon)

        current_performance = performance(bestMdl,x_test,y_test);
        Mdl = newModel(x_train, y_train, box_constraint, epsilon + epsilon_step , polyO);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if higher epsilon value has better performance
        if new_performance < current_performance

            epsilon = epsilon + epsilon_step;
            best_performance = new_performance;
            goUp = true;
            bestMdl = Mdl;
            printAccept(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);
        else
            printReject(iteration, new_performance, best_performance, box_constraint, polyO, epsilon + epsilon_step, supportVectors1, supportVectors2);
            improved_epsilon = false;
        end
    end

    if (goUp == false)

        improved_epsilon = true;
        while (improved_epsilon)
            if ((epsilon - epsilon_step) <= 0)
                break
            end

            current_performance = performance(bestMdl,x_test,y_test);
            Mdl = newModel(x_train, y_train, box_constraint, epsilon - epsilon_step, polyO);
            iteration = iteration + 1;
            new_performance = performance(Mdl,x_test,y_test);
            
            selectedVectors = size(Mdl.SupportVectors);
            supportVectors1 = selectedVectors(1);
            supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

            if new_performance < current_performance

                epsilon = epsilon - epsilon_step;
                best_performance = new_performance;
                bestMdl = Mdl;
                printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);

            else 
                printReject(iteration, new_performance, best_performance, box_constraint, polyO, epsilon - epsilon_step, supportVectors1, supportVectors2);
                improved_epsilon = false;
            end
        end
    end

    fprintf("\nEpsilon local minimum found: %f with accuracy: %f", epsilon, best_performance);

    %% BOX CONSTRAINT HILL CLIMBING

    % First, find best out of five random values for the parameter

    for e = 1:4
        current_performance = performance(bestMdl,x_test,y_test);
        temp_box_constraint = randi([1 15],1);
        Mdl = newModel(x_train, y_train, temp_box_constraint, epsilon , polyO);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if new box constraint value has better performance
        if new_performance < current_performance

            box_constraint = temp_box_constraint;
            best_performance = new_performance;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);

        else
            printReject(iteration, new_performance, best_performance, temp_box_constraint, polyO, epsilon, supportVectors1, supportVectors2);
        end
    end

    goUp = false;

    while (improved_boxConstraint)

        current_performance = performance(bestMdl,x_test,y_test);
        Mdl = newModel(x_train, y_train, box_constraint + box_constraint_step, epsilon, polyO);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;
        
        % if higher epsilon value has better performance
        if new_performance < current_performance

            box_constraint = box_constraint + box_constraint_step;
            best_performance = new_performance;
            goUp = true;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);

        else
            printReject(iteration, new_performance, best_performance, box_constraint + box_constraint_step, polyO, epsilon, supportVectors1, supportVectors2);
            improved_boxConstraint = false;
        end
    end

    if (goUp == false)

        improved_boxConstraint = true;
        while (improved_boxConstraint)
            if ((box_constraint - box_constraint_step) < 1)
                break
            end

            current_performance = performance(bestMdl,x_test,y_test);
            Mdl = newModel(x_train, y_train, box_constraint - box_constraint_step, epsilon, polyO);
            iteration = iteration + 1;
            new_performance = performance(Mdl,x_test,y_test);

            selectedVectors = size(Mdl.SupportVectors);
            supportVectors1 = selectedVectors(1);
            supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

            if new_performance < current_performance

                box_constraint = box_constraint - box_constraint_step;
                best_performance = new_performance;
                bestMdl = Mdl;
                printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon,  supportVectors1, supportVectors2);

            else 
                printReject(iteration, new_performance, best_performance, box_constraint - box_constraint_step, polyO, epsilon, supportVectors1, supportVectors2);
                improved_boxConstraint = false;
            end
        end
    end

    fprintf("\nBox constraint local minimum found: %f with accuracy: %f", box_constraint, best_performance);

    %% POLYNOMIAL ORDER HILL CLIMBING

    % First, find best out of five random values for the parameter

    for e = 1:4
        current_performance = performance(bestMdl,x_test,y_test);
        temp_polyO = randi([1 10],1);
        Mdl = newModel(x_train, y_train, box_constraint, epsilon , temp_polyO);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;
        
        % if new epsilon value has better performance
        if new_performance < current_performance

            polyO = temp_polyO;
            best_performance = new_performance;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);

        else
            printReject(iteration, new_performance, best_performance, box_constraint, temp_polyO, epsilon, supportVectors1, supportVectors2);
        end
    end

    goUp = false;

    while (improved_polyO)

        current_performance = performance(bestMdl,x_test,y_test);
        Mdl = newModel(x_train, y_train, box_constraint, epsilon, polyO + polyO_step);
        iteration = iteration + 1;
        new_performance = performance(Mdl,x_test,y_test);

        selectedVectors = size(Mdl.SupportVectors);
        supportVectors1 = selectedVectors(1);
        supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

        % if higher epsilon value has better performance
        if new_performance < current_performance

            polyO = polyO + polyO_step;
            best_performance = new_performance;
            goUp = true;
            bestMdl = Mdl;
            printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);

        else
            printReject(iteration, new_performance, best_performance, box_constraint, polyO + polyO_step, epsilon, supportVectors1, supportVectors2);
            improved_polyO = false;
        end
    end

    if (goUp == false)

        improved_polyO = true;
        while (improved_polyO)
            if ((polyO - polyO_step) <= 1)
                break
            end

            current_performance = performance(bestMdl,x_test,y_test);
            Mdl = newModel(x_train, y_train, box_constraint, epsilon, polyO - polyO_step);
            iteration = iteration + 1;
            new_performance = performance(Mdl,x_test,y_test);

            selectedVectors = size(Mdl.SupportVectors);
            supportVectors1 = selectedVectors(1);
            supportVectors2 = (selectedVectors(1) / isSupportVector(1))*100;

            if new_performance < current_performance

                polyO = polyO - polyO_step;
                best_performance = new_performance;
                bestMdl = Mdl;
                printBest(iteration, new_performance, best_performance, box_constraint, polyO, epsilon, supportVectors1, supportVectors2);

            else 
                printReject(iteration, new_performance, best_performance, box_constraint, polyO + polyO_step, epsilon, supportVectors1, supportVectors2);
                improved_polyO = false;
            end
        end
    end

    fprintf("\nPolynomial order local minimum found: %f with accuracy: %f", polyO, best_performance);

    fprintf('\n\nFINAL RESULT: \n');
    fprintf('\nBest Performance: %f \nBest box constraint: %f \nBest polynomial order: %f \nBest epsilon: %f ', best_performance, box_constraint, polyO, epsilon);

    predictedLabels = predict(bestMdl,x_test);
    modelAccuracy = sum((round(predictedLabels) == y_test))/length(y_test)*100;

    fprintf('\nAccuracy for rounded results (using percentages): %f', modelAccuracy);

end

% SVM Functions
function Mdl = newModel(X, Y, b, e, p)
    Mdl = fitrsvm(X, Y,'KernelFunction','polynomial', 'BoxConstraint', b, 'Epsilon', e, 'PolynomialOrder', p);
end

function Mdl = newLinearModel(X, Y, e)
    Mdl = fitrsvm(X, Y,'KernelFunction','linear', 'Epsilon', e);
end

function Mdl = newGausModel(X, Y, b, e, s)
    Mdl = fitrsvm(X, Y,'KernelFunction','rbf', 'BoxConstraint', b, 'Epsilon', e, 'KernelScale', s);
end

% Evaluation function

function err = performance(Mdl,x_test,y_test)
    %   PERFORMANCE Returns the accuracy of Mdl model's predictions on the 
    %   test data compared to validation labels
    %   It's calculated by rounding each of the model predictions and comparing
    %   them with the actual validation data; returned as percentage
    
    predictedLabels = predict(Mdl,x_test); 
    %modelAccuracy = sum((round(predictedLabels) == y_test))/length(y_test)*100;
    err = immse(predictedLabels,y_test); % performance function changed to mean-squared error (now its minimisation)
end

% Formatting for output
function printBest = printBest(a, b, c, d, e, f, g, h)
 fprintf('\n|       %i | %f |     best |     %f |      %f |   %f |    %f |         %f |     %f |', a, b, c, d, e, f, g, h);
end

function printAccept = printAccept(a, b, c, d, e, f, g, h)
 fprintf('\n|       %i | %f |   accept |     %f |      %f |   %f |    %f |         %f |     %f |', a, b, c, d, e, f, g, h);
end

function printReject = printReject(a, b, c, d, e, f, g, h)
 fprintf('\n|       %i | %f |   reject |     %f |      %f |   %f |    %f |         %f |     %f |', a, b, c, d, e, f, g, h);
end